from typing import NewType

CommitHash = NewType("CommitHash", str)
